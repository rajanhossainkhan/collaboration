-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 22, 2016 at 09:10 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `collaboration_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `email_admin` varchar(200) DEFAULT NULL,
  `reply_to` varchar(200) DEFAULT NULL,
  `email_from` varchar(200) DEFAULT NULL,
  `globalaccess` varchar(50) DEFAULT NULL,
  `groupaccess` varchar(50) DEFAULT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `usergroup` varchar(50) DEFAULT NULL,
  `project_order` varchar(200) DEFAULT NULL,
  `task_order` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`email_admin`, `reply_to`, `email_from`, `globalaccess`, `groupaccess`, `owner`, `usergroup`, `project_order`, `task_order`) VALUES
('rajanhossainkhan@gmail.com', NULL, NULL, 'checked="checked"', '', NULL, NULL, 'ORDER BY name', 'ORDER BY name');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `company` varchar(100) DEFAULT NULL,
  `tel_home` varchar(100) DEFAULT NULL,
  `gsm` varchar(100) DEFAULT NULL,
  `fax` varchar(100) DEFAULT NULL,
  `tel_business` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `postal` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `notes` text,
  `email` varchar(100) DEFAULT NULL,
  `added_by` int(10) UNSIGNED NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(10) UNSIGNED NOT NULL,
  `taskid` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `firstname`, `lastname`, `company`, `tel_home`, `gsm`, `fax`, `tel_business`, `address`, `postal`, `city`, `notes`, `email`, `added_by`, `date`, `user_id`, `taskid`) VALUES
(1, 'Rajan', 'Hossain', '', '', '', '', '', '', '', '', '', '', 1, '2016-12-20 12:08:05', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `contacts_tasks`
--

CREATE TABLE `contacts_tasks` (
  `contact_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(10) UNSIGNED NOT NULL,
  `fileid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `hashid` varchar(200) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `size` bigint(20) NOT NULL DEFAULT '0',
  `description` text,
  `uploaded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uploader` int(10) UNSIGNED NOT NULL,
  `mime` varchar(200) DEFAULT NULL,
  `taskid` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `fileid`, `hashid`, `filename`, `size`, `description`, `uploaded`, `uploader`, `mime`, `taskid`) VALUES
(2, 2, '72b46107f8ad63b4dd7dc06e076827d4c47ec6e9', 'propec-business-improvement-coaching-for-improvement-workshop.jpg', 172027, '', '2016-12-04 07:30:37', 1, 'image/jpeg', 2),
(3, 3, 'c6eb37e687ca587e4fc6f8f1701f76416dae8b13', 'dec_shot.JPG', 146650, '', '2016-12-15 08:43:52', 1, 'image/jpeg', 4),
(4, 4, '5fb5faaf6f38f0bfe36a97d86a9611e031ab177f', 'shot_kkkl.JPG', 175667, '', '2016-12-15 08:43:52', 1, 'image/jpeg', 4),
(5, 5, '33a1d838141d2001cc48ce650e8ee8bb8aa2dce2', 'Initial_System_Model.docx', 186140, '', '2016-12-20 04:05:49', 1, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 4);

-- --------------------------------------------------------

--
-- Table structure for table `forum`
--

CREATE TABLE `forum` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL,
  `taskid` int(10) UNSIGNED NOT NULL,
  `posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edited` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `text` text,
  `userid` int(10) UNSIGNED NOT NULL,
  `usergroupid` int(10) UNSIGNED NOT NULL,
  `sequence` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `forum`
--

INSERT INTO `forum` (`id`, `parent`, `taskid`, `posted`, `edited`, `text`, `userid`, `usergroupid`, `sequence`) VALUES
(1, 0, 1, '2016-10-02 14:19:49', '2016-10-02 14:19:49', 'I think we can do use this webcollab program as our wiki here.', 1, 0, 0),
(2, 1, 1, '2016-10-02 14:23:45', '2016-10-02 14:23:45', 'Okay agreed.', 1, 0, 0),
(3, 0, 1, '2016-10-03 02:44:34', '2016-10-03 02:44:34', 'This is a test comment', 1, 0, 0),
(4, 3, 1, '2016-10-03 02:44:46', '2016-10-03 02:44:46', 'This is a reply', 1, 0, 0),
(5, 0, 1, '2016-10-03 02:45:09', '2016-10-03 02:45:09', 'Tis is a test', 1, 1, 0),
(6, 0, 1, '2016-10-30 05:52:34', '2016-10-30 05:52:34', 'sfnidgvdfnb', 1, 1, 0),
(7, 5, 1, '2016-10-30 05:52:53', '2016-10-30 05:52:53', 'sfrignrjfgb', 1, 1, 0),
(8, 2, 1, '2016-10-30 11:20:23', '2016-10-30 11:20:23', 'owaw, this is great', 1, 0, 0),
(9, 8, 1, '2016-12-04 06:45:37', '2016-12-04 06:45:37', 'seterdyhdtfhjft', 1, 0, 0),
(10, 0, 2, '2016-12-04 07:30:20', '2016-12-04 07:30:20', 'Test', 1, 1, 0),
(11, 0, 2, '2016-12-04 07:30:54', '2016-12-04 07:30:54', 'ascsdvdfbdx', 1, 0, 0),
(12, 0, 4, '2016-12-15 08:40:26', '2016-12-15 08:40:26', 'gciudshvdfhv.bfbg', 1, 0, 0),
(13, 0, 4, '2016-12-15 08:41:20', '2016-12-15 08:41:20', 'efjslgjfd;bjfg;lnb', 1, 1, 0),
(14, 12, 4, '2016-12-15 08:41:29', '2016-12-15 08:41:29', 'hcdvndflbnvdflb', 1, 0, 0),
(15, 12, 4, '2016-12-15 08:41:44', '2016-12-15 08:41:44', 'ax ajkscbkdsbv f', 1, 0, 0),
(16, 15, 4, '2016-12-15 08:41:56', '2016-12-15 08:41:56', 'hxzkbv xcln kcv', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE `logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `session_key` varchar(100) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `lastaccess` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `logins`
--

INSERT INTO `logins` (`id`, `user_id`, `session_key`, `ip`, `lastaccess`, `token`) VALUES
(33, 1, 'XXXX', '::1', '2016-12-21 10:31:00', NULL),
(34, 2, '7ab124b41a9b95acc29a40a3cb69568d3cc32273', '::1', '2016-12-21 10:31:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempt`
--

CREATE TABLE `login_attempt` (
  `name` varchar(100) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `last_attempt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `maillist`
--

CREATE TABLE `maillist` (
  `email` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `seen`
--

CREATE TABLE `seen` (
  `taskid` int(10) UNSIGNED NOT NULL,
  `userid` int(10) UNSIGNED NOT NULL,
  `time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seen`
--

INSERT INTO `seen` (`taskid`, `userid`, `time`) VALUES
(1, 1, '2016-12-21 04:27:12'),
(2, 1, '2016-12-20 11:55:56'),
(3, 1, '2016-12-20 11:49:53'),
(4, 1, '2016-12-20 11:54:05'),
(5, 1, '2016-12-15 08:39:47'),
(6, 1, '2016-12-20 08:17:57');

-- --------------------------------------------------------

--
-- Table structure for table `site_name`
--

CREATE TABLE `site_name` (
  `manager_name` varchar(100) DEFAULT NULL,
  `abbr_manager_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_name`
--

INSERT INTO `site_name` (`manager_name`, `abbr_manager_name`) VALUES
('WebCollab Project Management', 'WebCollab');

-- --------------------------------------------------------

--
-- Table structure for table `taskgroups`
--

CREATE TABLE `taskgroups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taskgroups`
--

INSERT INTO `taskgroups` (`id`, `name`, `description`) VALUES
(1, 'SDA Task Group 1', 'This is to test primary functionality');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `text` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edited` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `owner` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `creator` int(10) UNSIGNED NOT NULL,
  `finished_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `projectid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `deadline` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `priority` tinyint(4) NOT NULL DEFAULT '2',
  `status` varchar(20) NOT NULL DEFAULT 'created',
  `taskgroupid` int(10) UNSIGNED NOT NULL,
  `lastforumpost` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `usergroupid` int(10) UNSIGNED NOT NULL,
  `globalaccess` varchar(5) NOT NULL DEFAULT 't',
  `groupaccess` varchar(5) NOT NULL DEFAULT 'f',
  `lastfileupload` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `completed` tinyint(4) NOT NULL DEFAULT '0',
  `completion_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `archive` tinyint(4) NOT NULL DEFAULT '0',
  `sequence` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `parent`, `name`, `text`, `created`, `edited`, `owner`, `creator`, `finished_time`, `projectid`, `deadline`, `priority`, `status`, `taskgroupid`, `lastforumpost`, `usergroupid`, `globalaccess`, `groupaccess`, `lastfileupload`, `completed`, `completion_time`, `archive`, `sequence`) VALUES
(1, 0, 'SDA Project - Elearning', 'We are developing an e-learning online solution.', '2016-10-02 14:18:42', '2016-10-02 14:28:29', 1, 1, '2016-10-02 14:18:42', 1, '2016-10-02 02:00:00', 3, 'active', 0, '2016-12-04 06:45:37', 1, 't', 't', '2016-12-04 06:36:33', 0, '2016-10-02 14:18:42', 0, 1),
(2, 0, 'Assignment Week 1', 'asfsdxbdfgnhvmjb,mn,.k,.k/.l./;.cvxcvbvcbvnbmnvbm', '2016-12-04 07:29:53', '2016-12-20 04:04:42', 1, 1, '2016-12-04 07:29:53', 2, '2016-12-19 02:00:00', 3, 'active', 0, '2016-12-04 07:30:54', 1, 't', 'f', '2016-12-04 07:30:37', 0, '2016-12-04 07:29:53', 0, 1),
(3, 2, 'Task 1 by Hudah', '', '2016-12-04 07:31:36', '2016-12-04 07:31:36', 1, 1, '2016-12-04 07:31:36', 2, '2016-12-12 02:00:00', 2, 'created', 1, '2016-12-04 07:31:36', 1, 't', 'f', '2016-12-04 07:31:36', 0, '2016-12-04 07:31:36', 0, 0),
(4, 0, 'SDA Project 1', 'We Setup blah blah', '2016-12-15 08:30:48', '2016-12-15 08:30:48', 1, 1, '2016-12-15 08:30:48', 4, '2016-12-25 02:00:00', 3, 'active', 0, '2016-12-15 08:41:56', 1, 't', 'f', '2016-12-20 04:05:49', 100, '2016-12-20 08:17:57', 0, 0),
(5, 4, 'Requirement Analysis', '', '2016-12-15 08:32:52', '2016-12-15 08:39:47', 1, 1, '2016-12-15 08:39:47', 4, '2016-12-22 02:00:00', 3, 'done', 1, '2016-12-15 08:32:52', 1, 't', 't', '2016-12-15 08:32:52', 0, '2016-12-15 08:32:52', 0, 1),
(6, 5, 'Functiojal Requirement', '', '2016-12-15 08:35:53', '2016-12-20 08:17:57', 1, 1, '2016-12-20 08:17:57', 4, '2016-12-22 02:00:00', 2, 'done', 1, '2016-12-15 08:35:53', 0, 't', 'f', '2016-12-15 08:35:53', 0, '2016-12-15 08:35:53', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `token` varchar(100) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `userid` int(10) UNSIGNED NOT NULL,
  `lastaccess` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usergroups`
--

CREATE TABLE `usergroups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `private` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usergroups`
--

INSERT INTO `usergroups` (`id`, `name`, `description`, `private`) VALUES
(1, 'SDA Group 1', 'This is to test primary functionality', 0);

-- --------------------------------------------------------

--
-- Table structure for table `usergroups_users`
--

CREATE TABLE `usergroups_users` (
  `usergroupid` int(10) UNSIGNED NOT NULL,
  `userid` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usergroups_users`
--

INSERT INTO `usergroups_users` (`usergroupid`, `userid`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `fullname` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `admin` varchar(5) NOT NULL DEFAULT 'f',
  `private` tinyint(4) NOT NULL DEFAULT '0',
  `guest` tinyint(4) NOT NULL DEFAULT '0',
  `deleted` varchar(5) NOT NULL DEFAULT 'f',
  `locale` varchar(10) NOT NULL DEFAULT 'en'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `fullname`, `password`, `email`, `admin`, `private`, `guest`, `deleted`, `locale`) VALUES
(1, 'admin', 'Rajan Hossain', '$2y$10$jGjoPAD2CM7/B6PG5nueZuV44BG/9ZGpP3LNXHG3rmyV7eTROfNfe', 'rajanhossainkhan@gmail.com', 't', 0, 0, 'f', 'en'),
(2, 'raj', 'Theevan Raj', '$2y$10$ju03aK3KgiJV9cOYiciGE.Q4kzkyZ2VD5dQAnHkMl6iyAR1jC37/S', 'raj@email.com', 'f', 0, 0, 'f', 'en');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `taskid` (`taskid`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `taskid` (`taskid`);

--
-- Indexes for table `forum`
--
ALTER TABLE `forum`
  ADD PRIMARY KEY (`id`),
  ADD KEY `taskid` (`taskid`),
  ADD KEY `edited` (`edited`);

--
-- Indexes for table `logins`
--
ALTER TABLE `logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `session_key` (`session_key`(10),`user_id`),
  ADD KEY `lastaccess` (`lastaccess`);

--
-- Indexes for table `seen`
--
ALTER TABLE `seen`
  ADD KEY `taskid` (`taskid`,`userid`);

--
-- Indexes for table `taskgroups`
--
ALTER TABLE `taskgroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`(10));

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner` (`owner`),
  ADD KEY `parent` (`parent`),
  ADD KEY `name` (`name`(10)),
  ADD KEY `projectid` (`projectid`),
  ADD KEY `taskgroupid` (`taskgroupid`),
  ADD KEY `deadline` (`deadline`),
  ADD KEY `status` (`status`,`parent`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD KEY `token` (`token`);

--
-- Indexes for table `usergroups`
--
ALTER TABLE `usergroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`(10));

--
-- Indexes for table `usergroups_users`
--
ALTER TABLE `usergroups_users`
  ADD KEY `userid` (`userid`,`usergroupid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fullname` (`fullname`(10));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `forum`
--
ALTER TABLE `forum`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `logins`
--
ALTER TABLE `logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `taskgroups`
--
ALTER TABLE `taskgroups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `usergroups`
--
ALTER TABLE `usergroups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
