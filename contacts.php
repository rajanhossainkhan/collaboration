<?php
/*
 Author: Rajan Hossain
 Page: Ccontacts list 
*/


require_once('path.php');
require_once(BASE.'includes/security.php' );
include_once(BASE.'includes/screen.php' );

//
// The action handler
//
if(isset($_POST['action'] ) ) {
  $action = $_POST['action'];
}
elseif(isset($_GET['action'] ) ) {
  $action = $_GET['action'];
}
else {
  error('Contacts action handler', 'No request given' );
}

//what do you want to contact today =]
switch($action ) {

  //gives a window and some options to do to the poor 'old contact manager
  case 'show':
    create_top($lang['show_contact'], 0, 'contact-show' );
    include(BASE.'includes/mainmenu.php' );
    include(BASE.'contacts/contact_menubox.php' );
    goto_main();
    include(BASE.'contacts/contact_show.php' );
    create_bottom();
    break;

  //gives a window and some options to do to the poor 'old contact manager
  case 'add':
    create_top($lang['add_contact'], 0, 'contact-add', 1 );
    include(BASE.'includes/mainmenu.php' );
    include(BASE.'contacts/contact_menubox.php' );
    goto_main();
    include(BASE.'contacts/contact_add.php' );
    create_bottom();
    break;

  case 'edit':
    create_top($lang['edit_contact'], 0, 'contact-edit', 1 );
    include(BASE.'includes/mainmenu.php' );
    include(BASE.'contacts/contact_menubox.php' );
    goto_main();
    include(BASE.'contacts/contact_edit.php' );
    create_bottom();
    break;

  case 'submit_add':
  case 'submit_edit':
  case 'submit_delete':
    include(BASE.'contacts/contact_submit.php' );
    break;

  //error case
  default:
    error('Contacts action handler', 'Invalid request given') ;
    break;
}


?>