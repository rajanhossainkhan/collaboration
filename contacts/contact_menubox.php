<?php
/*
  Author : Rajan  Hossain

*/

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//initialise variables
$content = '';
$company = '';

if( @safe_integer($_GET['taskid']) ) {

  $taskid = $_GET['taskid'];

  //get task details
  require_once(BASE.'includes/details.php' );

  //check usergroup rights
  require_once(BASE.'includes/usergroup_security.php' );
  usergroup_check($taskid );

  //get task contacts
  $q = db_prepare('SELECT id, firstname, lastname, company FROM '.PRE.'contacts
                          WHERE taskid=? OR taskid=? ORDER BY company, lastname' );
  db_execute($q, array($taskid, $TASKID_ROW['projectid'] ) );
  $add  = '&amp;taskid='.$taskid;
}
else {
  //get all contacts
  $q = db_query('SELECT id, firstname, lastname, company FROM '.PRE.'contacts 
                          WHERE taskid=0 ORDER BY company, lastname');
  $add  = '';
}

$content .= "<ul class=\"menu\">\n";

//show all contacts
for( $i=0 ; $row = @db_fetch_array($q, $i ) ; ++$i) {

  if( $row['company'] != '' ) {
     if ($row['company'] != $company ){
       $content .= "<li>".box_shorten($row['company'] )."</li>";
     }
     $show = box_shorten($row['lastname'] ).", ".mb_strtoupper(mb_substr($row['firstname'], 0, 1 ) ).".";
     $content .= "<li><a href=\"contacts.php?x=".X."&amp;action=show&amp;contactid=".$row['id']."\">".$show."</a></li>";
     $company =  $row['company'];
   }
   else {
     $show = box_shorten($row['lastname'] ).", ".mb_strtoupper(mb_substr($row['firstname'], 0, 1 ) ).".";
     $content .= "<li><a href=\"contacts.php?x=".X."&amp;action=show&amp;contactid=".$row['id']."\">".$show."</a></li>";
   }
}

db_free_result($q );

$content .= "</ul>\n";

if($i == 0 ) {
  $content = '';
}

//the add button
if(! GUEST ){
  $content .= "<div style=\"margin-top: 20px\"><span class=\"textlink\">[<a href=\"contacts.php?x=".X."&amp;action=add".$add."\">".$lang['add_contact']."</a>]</span></div>\n";
}

//show the box
new_box($lang['contacts'], $content, 'boxdata-menu', 'head-menu', 'boxstyle-menu' );

?>