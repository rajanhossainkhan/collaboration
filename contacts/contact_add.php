<?php
/*
  Author : Rajan  Hossain

*/

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

if(GUEST ) {
  error('Contact add', 'Guest not authorised' );
}

$taskid = ( @safe_integer($_GET['taskid']) ) ? $_GET['taskid'] : 0 ;

$content = "<form method=\"post\" action=\"contacts.php\" onsubmit=\"return fieldCheck('lastname', 'firstname' )\">\n".
           "<fieldset><input type=\"hidden\" name=\"x\" value=\"".X."\" />\n".
           "<input type=\"hidden\" name=\"action\" value=\"submit_add\" />\n".
           "<input type=\"hidden\" name=\"taskid\" value=\"".$taskid."\" />\n".
           "<input type=\"hidden\" id=\"alert_field\" name=\"alert\" value=\"".$lang['missing_field_javascript']."\" />\n".
           "<input type=\"hidden\" id=\"url\" name=\"url\" value=\"".$lang['url_javascript']."\" />\n".
           "<input type=\"hidden\" id=\"image_url\" name=\"image_url\" value=\"".$lang['image_url_javascript']."\" /></fieldset>\n".
           "<table class=\"celldata\">\n".
           "<tr><td><i>".$lang['firstname']."</i></td><td><input id=\"firstname\" type=\"text\" name=\"firstname\" class=\"size\" /><script type=\"text/javascript\">document.getElementById('firstname').focus();</script></td></tr>\n".
           "<tr><td><i>".$lang['lastname']."</i></td><td><input id=\"lastname\" type=\"text\" name=\"lastname\" class=\"size\" /></td></tr>\n".
           "<tr><td><i>".$lang['company']."</i></td><td><input type=\"text\" name=\"company\" class=\"size\" /></td></tr>\n".
           "<tr><td><i>".$lang['home_phone']."</i></td><td><input type=\"text\" name=\"tel_home\" class=\"size\" /></td></tr>\n".
           "<tr><td><i>".$lang['mobile']."</i></td><td><input type=\"text\" name=\"gsm\" class=\"size\" /></td></tr>\n".
           "<tr><td><i>".$lang['bus_phone']."</i></td><td><input type=\"text\" name=\"tel_business\" class=\"size\" /></td></tr>\n".
           "<tr><td><i>".$lang['fax']."</i></td><td><input type=\"text\" name=\"fax\" class=\"size\" /></td></tr>\n".
           "<tr><td><i>".$lang['address']."</i></td><td><input type=\"text\" name=\"address\" class=\"size\" /></td></tr>\n".
           "<tr><td><i>".$lang['postal']."</i></td><td><input type=\"text\" name=\"postal\" class=\"size\" /></td></tr>\n".
           "<tr><td><i>".$lang['city']."</i></td><td><input type=\"text\" name=\"city\" class=\"size\" /></td></tr>\n".
           "<tr><td><i>".$lang['email_contact']."</i></td><td><input type=\"text\" name=\"email\" class=\"size\" /></td></tr>\n".
           "</table>\n".
           "<p><i>".$lang['notes']."</i><br />\n".
           "<script type=\"text/javascript\"> edToolbar('notes');</script>".
           "<textarea name=\"notes\" id=\"notes\" rows=\"6\" cols=\"50\"></textarea></p>\n".
           "<p><input type=\"submit\" value=\"".$lang['add_contact']."\"/></p>\n".
           "</form>\n";

new_box( $lang['contact_info'], $content );

new_box($lang['info'], $lang['contact_add_info'] );

?>
