<?php
/*
  Author: Rajan Hossain
  Page: Setup file
*/

require_once('path.php');
require_once(BASE.'setup/security_setup.php' );

//
// The action handler
//
if(isset($_POST['action'] ) ) {
  $action = $_POST['action'];
}
elseif(isset($_GET['action'] ) ) {
  $action = $_GET['action'];
}
else {
  error('Setup action handler', 'No request given' );
}

//what do you want to task today =]
switch($action ) {

case 'setup1':
    include(BASE.'setup/setup_setup1.php' );
    break;

case 'setup2':
    include(BASE.'setup/setup_setup2.php' );
    break;

  case 'setup3':
    include(BASE.'setup/setup_setup3.php' );
    break;

  case 'setup4':
    include(BASE.'setup/setup_setup4.php' );
    break;

  case 'setup5':
    include(BASE.'setup/setup_setup5.php' );
    break;

  case 'setup6':
    include(BASE.'setup/setup_setup6.php' );
    break;

  case 'setup7':
    include(BASE.'setup/setup_setup7.php' );
    break;

  case 'build':
    include(BASE.'setup/setup_db_build.php' );
    break;

  //error case
  default:
    error('File action handler', 'Invalid request given' );
    break;
}

?>