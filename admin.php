<?php
/*
  Author: Rajan Hossain
  Page: Admin Configuration
*/

require_once('path.php');
require_once(BASE.'includes/security.php' );
include_once(BASE.'includes/screen.php' );

if(! ADMIN ){
  warning('Admin action handler', 'This area for admins only' );
}

//
// The action handler
//
if(isset($_POST['action'] ) ) {
  $action = $_POST['action'];
}
elseif(isset($_GET['action'] ) ) {
  $action = $_GET['action'];
}
else {
  error('Admin action handler', 'No request given' );
}

switch ($action ) {

  case 'admin':
    create_top($lang['admin_config'], 0, 'admin-config', 2 );
    include(BASE.'includes/mainmenu.php' );
    goto_main();
    include(BASE.'admin/admin_config_edit.php' );
    create_bottom();
    break;

  case 'submit':
    include(BASE.'admin/admin_config_submit.php' );
    break;

  //error case
  default:
    error('Admin action handler', 'Invalid request given') ;
    break;

}

?>
