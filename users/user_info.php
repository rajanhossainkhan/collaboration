<?php
/*
  Author : Rajan  Hossain
*/

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

include_once('lang/lang_long.php' );

//admins only
if(! ADMIN ){
  error('Unauthorised access', 'This function is for admins only.' );
}

$content = $user_info;

new_box($lang['manage_users'], $content, 'boxdata-normal', 'head-normal', 'boxstyle-short' );

?>