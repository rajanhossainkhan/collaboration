<?php
/*
  Author : Rajan  Hossain
*/

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//
// Create drop down box with available languages
//
function user_locale($locale) {

  $content = '';

  //get list of languages
  include_once(BASE.'lang/lang_list.php' );

  //start menu box
  $content .= "<tr><td>Language:</td><td><select name=\"locale\">\n";

  foreach($lang_list as $key => $value ) {

    $content .= "<option value=\"".$key."\" ";

    //highlight current language
    if($locale == $key) {
      $content .= " selected=\"selected\"";
    }

    $content .= ">".$value."</option>\n";
  }

  $content .= "</select></td></tr>\n";

 return $content;
}

//
// Check validity of language string
//
function user_locale_check($locale ) {

  //get list of languages
  include_once(BASE.'lang/lang_list.php' );

  if(! isset($lang_list[$locale ] ) ) {
    warning("User submit", "Language file ".$locale." does not exist" );
  }

  return $locale;
}

//
// Function to generate blowfish hashes
//
function pass_hash($password ) {
  
  $hash = password_hash($password, PASSWORD_BCRYPT );
  
  if(strlen($hash ) < 13 ) {
    //blowfish will give a random string of less than 13 characters in error condition
    error('Password setting error', 'Password hash algorithm failed. Transaction cancelled' );
  }
  
  return $hash;
}
?>