<?php
/*
  Author : Rajan  Hossain
*/

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//first check if we are admin
if(! ADMIN ){
  return;
}

//check for deleted users
$q = db_query('SELECT COUNT(*) FROM '.PRE.'users WHERE deleted=\'t\'' );
if( ! db_result($q, 0, 0 ) ) {
  $content = "<small>".$lang['no_deleted_users']."</small>";
  new_box($lang['deleted_users'], $content );
  return;
}

//query
$q = db_query('SELECT id, fullname FROM '.PRE.'users WHERE deleted=\'t\' ORDER BY fullname' );

$content = "<table class=\"celldata\">\n";

//show them
for($i=0 ; $row = @db_fetch_array($q, $i ) ; ++$i ) {
  $content .= "<tr class=\"grouplist\"><td><a href=\"users.php?x=".X."&amp;action=show&amp;userid=".$row['id']."\">".$row['fullname']."</a></td>\n".
              "<td><span class=\"textlink\">".
              "[<a href=\"users.php?x=".X."&amp;action=edit_del&amp;userid=".$row['id']."\">".$lang['edit']."</a>]".
              "</span></td></tr>\n";
}

$content .= "</table>";

//show it
new_box($lang['deleted_users'], $content );

?>