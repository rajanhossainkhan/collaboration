<?php
/*
    Author: Rajan Hossain
    Date: 15-11-2016
    File Desc: Project description
 */

require_once('path.php');
require_once(BASE.'includes/security.php' );
include_once(BASE.'includes/screen.php' );

//make active projects and condensed projects 'sticky' with a persistent cookie
if(isset($_GET['active_only'] ) || isset($_GET['condensed'] ) ) {
  if(isset($_GET['active_only'] ) ) {
    $cookie_array['active_only'] = ($_GET['active_only']) ? 1 : 0;
  }
  if(isset($_GET['condensed'] ) ) {
    $cookie_array['condensed'] = ($_GET['condensed']) ? 1 : 0;
  }

  //sort into an array and implode
  foreach($cookie_array as $key => $value ) {
    $cookie_array_value[] = $key.'='.$value;
  }
  //key1=value1:key2=value2 , etc
  $cookie_value = implode(':', $cookie_array_value );
  setcookie('webcollab_sticky', $cookie_value, (time() + 86400*365*10 ) );
}

//start of page
create_top('', 0, 'project-list' );

  include(BASE.'includes/mainmenu.php' );
  include(BASE.'forum/forum_menubox.php' );
  if(! GUEST){
    include(BASE.'tasks/task_menubox.php' );
  }
  include(BASE.'users/user_menubox.php' );

  if(ADMIN ) {
    include(BASE.'taskgroups/taskgroup_menubox.php' );
    include(BASE.'usergroups/usergroup_menubox.php' );
    include(BASE.'admin/admin_config_menubox.php' );
    include(BASE.'files/file_menubox.php' );
  }
  else {
    include(BASE.'usergroups/usergroup_menubox.php' );
  }
  //include(BASE.'contacts/contact_menubox.php' );

//flip over to other frame
goto_main();
  include(BASE.'tasks/task_project_list.php' );

//finish page
create_bottom();

?>
