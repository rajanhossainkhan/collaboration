<?php
/*
Author: Rajan Hossain
Page: Login Page 
*/

require_once('path.php' );
require_once(BASE.'path_config.php' );
require_once(BASE_CONFIG.'config.php' );

include_once(BASE.'lang/lang.php' );
include_once(BASE.'includes/common.php' );
include_once(BASE.'includes/screen.php' );

//error condition
function secure_error($error='Login error', $redirect_time=0 ) {

  global $lang;

  $content = "<div style=\"text-align : center\">".$error."</div>";
  create_top($lang['login'], 1, 0, 0, $redirect_time );
  new_box($lang['error'], $content, 'boxdata-small', 'head-small' );

  if($redirect_time != 0) {
    $content = "<div style=\"text-align : center\"><a href=\"".BASE_URL."index.php\">".$lang['login_now']."</a></div>\n";
    new_box(sprintf($lang['redirect_sprt'], $redirect_time ), $content, 'boxdata-small', 'head-small' );
  }

  create_bottom();
  die;
}

//enable login
function enable_login($userid, $username, $ip='0.0.0.0', $taskid ) {

  //create session key
  //  openssl_random_pseudo_bytes() is preferred
  if(function_exists('openssl_random_pseudo_bytes' ) ) {
    //random key of 40 hex characters length
    $session_key = bin2hex(openssl_random_pseudo_bytes(20 ) );
  }
  else {
    //use Mersenne Twister algorithm (random number) to give up to 128 bits, then one-way hash to give session key
    $session_key = sha1(mt_rand().mt_rand().mt_rand().mt_rand() );
  }
  
  //remove the old login information
  $q = db_prepare('DELETE FROM '.PRE.'logins WHERE user_id=?' );
  @db_execute($q, array($userid ) );
  $q = db_prepare('DELETE FROM '.PRE.'login_attempt WHERE last_attempt < (now()-INTERVAL '.db_delim('20 MINUTE' ).') OR name=?' );
  @db_execute($q, array($username ) );
  @db_query('DELETE FROM '.PRE.'tokens WHERE lastaccess < (now()-INTERVAL '.db_delim(TOKEN_TIMEOUT.' MINUTE' ).')' );

  //log the user in
  $q = db_prepare('INSERT INTO '.PRE.'logins(user_id, session_key, ip, lastaccess ) VALUES (?, ?, ?, now() )' );
  @db_execute($q, array($userid, $session_key, $ip ) );

  //try and set a session cookie (if the browser will let us)
  $url = parse_url(BASE_URL );
  //use HTTP only to reduce XSS attacks (only in PHP 5.2.0+ )
  setcookie('webcollab_session', $session_key, 0, $url['path'], $url['host'], false, true );
  //(No need to record an error here if unsuccessful: the code will revert to URI session keys)

  //relocate the user to the main screen
  //(we use both URI session key and cookies initially - in case cookies don't work)
  if($taskid == 0 ) {
    header('Location: '.BASE_URL.'main.php?x='.$session_key );
  }
  else {
    header('Location: '.BASE_URL.'tasks.php?x='.$session_key.'&action=show&taskid='.$taskid );
  }
  die;
  return;
}

//perform login query
function login_query($username ) {

  //construct login query
  if(! ($q = db_prepare('SELECT id FROM '.PRE.'users WHERE name=? AND deleted=\'f\'', 0 ) ) ) {
    secure_error('Unable to connect to database.  Please try again later.' );
  }

  //database query
  if( ! @db_execute($q, array($username ), 0 ) ) {
    secure_error('Unable to connect to database.  Please try again later.' );
  }

  //no such user-password combination
  if( ! ($userid = @db_result($q, 0, 0) ) ) {
    secure_error('Access denied to unknown user \''.$username.'\'' );
  }
  else {
    return $userid;
  }
  return false;
}

//record recent login failures
function record_fail($username, $ip ) {

 global $lang;

  //record this login attempt
  $q = db_prepare('INSERT INTO '.PRE.'login_attempt(name, ip, last_attempt ) VALUES (?, ?, now() )' );
  db_execute($q, array($username, $ip ) );

  //wait 2 seconds then record an error
  sleep (2);
  secure_error($lang['no_login'], 15 );
  die;
}

//limit number of login attempts
function check_lockout($username ) {

  //count the number of recent failed login attempts
  if(! ($q = db_prepare('SELECT COUNT(*) FROM '.PRE.'login_attempt WHERE name=?
			      AND last_attempt > (now()-INTERVAL '.db_delim('10 MINUTE').') LIMIT 6', 0 ) ) ) {
    secure_error('Unable to connect to database.  Please try again later.' );
  }

  if( ! @db_execute($q, array($username ), 0 ) ) {
    secure_error('Unable to connect to database.  Please try again later.' );
  }

  $count_attempts = db_result($q, 0, 0 );

  //protect against password guessing attacks
  if($count_attempts > 4 ) {
    secure_error("Exceeded allowable number of login attempts.<br /><br />Account locked for 10 minutes." );
    die;
  }
  
  return true;
}

function password_upgrade($userid, $password ) {

  $hash = password_hash($password, PASSWORD_BCRYPT );

  //blowfish will give a random string of less than 13 characters in error condition
  if(strlen($hash ) < 13 ) return false;

  //update the password
  db_begin();

  $q = db_prepare('UPDATE '.PRE.'users SET password=? WHERE id=?' );
  db_execute($q, array($hash, $userid ) );

  db_commit();

  return true;
}

//
// MAIN LOGIN
//

//check and set taskid & nologin if required
$taskid  = (isset($_GET['taskid']) && @safe_integer($_GET['taskid']) ) ? $_GET['taskid'] : 0;
$nologin = (isset($_GET['nologin']) ) ? 1 : 0;

//secure variables
$content = '';
$q = '';
$row = '';
$hash = 'xxxx';
$salt = '';
$username = '0';
$password = '0';
$session_key = '';

//log ip address
if( ! ($ip = $_SERVER['REMOTE_ADDR'] ) ) {
  secure_error('Unable to determine ip address');
}

// 1. Password login authentication
if(isset($_POST['username']) && isset($_POST['password']) && strlen($_POST['username']) > 0 && strlen($_POST['password']) > 0 && ACTIVE_DIRECTORY != 'Y' ) {

  include_once(BASE.'database/database.php');

  $username = safe_data($_POST['username'] );

  //check for account locked
  check_lockout($username );
  
  //construct login query for username / password
  if(! ($q = db_prepare('SELECT id, password FROM '.PRE.'users WHERE name=? AND deleted=\'f\'', 0 ) ) ) {
    secure_error('Unable to connect to database.  Please try again later.' );
  }

  //database query
  if( ! @db_execute($q, array($username), 0 ) ) {
    secure_error('Unable to connect to database.  Please try again later.' );
  }
  
  //if user-password combination exists
  if($row = @db_fetch_array($q, 0 ) ) {

    //bcrypt encryption or SHA256 + salt (deprecated - used WebCollab 3.30 - 3.31 )
    if(strlen($row['password'] ) > 50 ){
      //verify password
      //if(password_verify($_POST['password'], $row['password'] ) ) {
        //check need for rehash (work factor changed or older SHA256 )
        // if(password_needs_rehash($row['password'], PASSWORD_BCRYPT ) ) {
        //   password_upgrade($row['id'], validate($_POST['password'] ) );
        // }
        //valid password -> continue
        enable_login($row['id'], $username, $ip, $taskid );
      //}
    }

    //fallback to older md5 encryption (deprecated - used WebCollab 1.01 - 3.30 )
    if(strlen($row['password'] ) < 35 ) {
      //verify password
      if($row['password'] === md5($_POST['password'] ) ) {
        //upgrade password now...
        password_upgrade($row['id'], validate($_POST['password'] ) );
        //valid password -> continue
        enable_login($row['id'], $username, $ip, $taskid );
      }
    }
  }

  //no such user-password combination
  record_fail($username, $ip);
}

// 2. Web authorisation
if(WEB_AUTH === 'Y' && isset($_SERVER['REMOTE_USER']) && (strlen($_SERVER['REMOTE_USER']) > 0 ) ) {

  include_once 'database/database.php';

  $username = safe_data($_SERVER['REMOTE_USER'] );

  if($userid = login_query($username ) ) {
    enable_login($userid, $username, $ip, $taskid );
  }
}

// 3. ACTIVE DIRECTORY login
if(ACTIVE_DIRECTORY == 'Y' && isset($_POST['username']) && isset($_POST['password']) && strlen($_POST['username']) > 0 && strlen($_POST['password']) > 0 ) {

  include_once(BASE.'database/database.php');

  $username = safe_data($_POST['username']);
  $password = safe_data($_POST['password'] );
  
  //check for account locked
  check_lockout($username );
  
  if(! $adconn = ldap_connect($AD_HOST, AD_PORT ) ) {
    secure_error('ACTIVE_DIRECTORY: Connection not successful.' );
  }

  ldap_set_option($adconn, LDAP_OPT_PROTOCOL_VERSION, 3 );

  if( ! $ldap_bind = ldap_bind($adconn, $username, $password ) ) {
    secure_error($lang['no_login'], 15 );
  }

  ldap_close($adconn);

  if($userid = login_query($username ) ) {
    enable_login($userid, $username, $ip, $taskid );
  }
  
  //record failure
  record_fail($username, $ip );
  
}

// 4. Continuation of session
if(isset($_COOKIE['webcollab_session'] ) && preg_match('/^[a-f\d]{32}$/i', $_COOKIE['webcollab_session'] ) && (! $nologin ) ) {
  //allow for continuation of session if a valid cookie is already set
  // if 'nologin' is set we have just been rejected by security.php

  include_once 'database/database.php';

  //check if session is valid and within time limits
  $q = db_prepare('SELECT COUNT(*) FROM '.PRE.'logins
                          WHERE session_key=?
                          AND lastaccess > (now()-INTERVAL '.db_delim(round(SESSION_TIMEOUT).' HOUR').')' );
  db_execute($q, array(safe_data($_COOKIE['webcollab_session']) ) );

  if(db_result($q, 0, 0 ) == 1 ) {

    //relocate to main screen, and let security.php do further checking on session validity
    if($taskid == 0 ) {
      header('Location: '.BASE_URL.'main.php' );
    }
    else {
      header('Location: '.BASE_URL.'tasks.php?action=show&taskid='.$taskid );
    }
    die;
  }
}

//
// LOGIN SCREEN
//


//create login screen
create_top($lang['login_screen'], 1, 'login', 0 );
$content = "<div style=\"text-align:center;\">\n";

echo '<center><img src="images/logo.jpg" alt="Elearning Logo" style="width:250px; height:100px; border-radius:5px; margin-top:10px;"/></center><hr>';

$content .= "<div style=''><p>".$lang['please_login'].":</p>\n".
	    "<form method=\"post\" action=\"index.php\">\n".
	    "<fieldset><input type=\"hidden\" name=\"taskid\" value=\"".$taskid."\" /></fieldset>\n".
	    "<table style=\"margin-left:auto; margin-right:auto;\">\n".
	    "<tr align=\"left\" ><td>".$lang['login'].": </td><td><input id=\"username\" class=\"size\" type=\"text\" name=\"username\" value=\"\" />".
	    "<script type=\"text/javascript\">document.getElementById('username').focus();</script></td></tr>\n".
	    "<tr align=\"left\" ><td>".$lang['password'].": </td><td><input type=\"password\" class=\"size\" name=\"password\" value=\"\" /></td></tr>\n".
	    "</table>\n".
	    "<p style=\"padding-top: 20px; padding-bottom: 20px\"><input type=\"submit\" value=\"".$lang['login_action']."\" /></p>\n".
	    "</form>\n";
$content .= "</div></div>\n";
echo "</div>";

//set box options
new_box($lang['login'], $content, 'boxdata-small', 'head-small' );
create_bottom();
?>