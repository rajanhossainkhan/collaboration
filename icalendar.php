<?php
/*
  Author: Rajan Hossain
  Page: calender file
*/

require_once('path.php');
require_once(BASE.'includes/security.php' );

//set flag
$local_login = true;

//
// The action handler
//
if(isset($_POST['action'] ) ) {
  $action = $_POST['action'];
}
elseif(isset($_GET['action'] ) ) {
  $action = $_GET['action'];
}
else {
  error('iCalendar action handler', 'No request given' );
}

//what do you want to do today =]
switch($action ) {

  case 'todo':
    include(BASE.'icalendar/icalendar_todo.php' );
    break;

  case 'project':
    include(BASE.'icalendar/icalendar_project.php' );
    break;

  case 'all':
    include(BASE.'icalendar/icalendar_all.php' );
    break;

  //error case
  default:
    error('Calendar action handler', 'Invalid request given' );
    break;
}

?>