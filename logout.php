<?php
/*
Author: Rajan Hossain
Page: Logout global fro whole systemn
*/

require_once('path.php' );
require_once(BASE.'includes/security.php' );

//log the user out by nulling their session key to an illegal key
//record preserved to allow time of last login to be recorded
$q = db_prepare('UPDATE '.PRE.'logins SET session_key=\'XXXX\' WHERE user_id=?' );
db_execute($q, array(UID ) );
$q = db_prepare('DELETE FROM '.PRE.'tokens WHERE userid=?' );
@db_execute($q, array(UID ) );


//clear session cookie
$url = parse_url(BASE_URL );
setcookie('webcollab_session', "", (time() - 60 * 60 * 24 * 5 ), $url['path'], $url['host'], false, true );
//cookie expires 5 days ago...

header('Location: '.BASE_URL.'index.php' );

?>
