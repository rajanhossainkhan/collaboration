<?php
/*
  Author : Rajan  Hossain
*/

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//admin only
if( ! ADMIN ){
  return;
}

//secure values
$content = '';

$content .= "<ul class=\"menu\">\n".
            "<li><a href=\"admin.php?x=".X."&amp;action=admin\">".$lang['admin_config']."</a></li>\n".
            "</ul>\n";

//show it
new_box( $lang['admin_config'], $content, 'boxdata-menu', 'head-menu', 'boxstyle-menu' );

?>
