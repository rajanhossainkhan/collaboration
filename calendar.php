<?php
/*
  Author: Rajan Hossain
  Page: Calender
*/

require_once('path.php');
require_once(BASE.'includes/security.php' );
include_once(BASE.'includes/screen.php' );

//
// The action handler
//
if(isset($_POST['action'] ) ) {
  $action = $_POST['action'];
}
elseif(isset($_GET['action'] ) ) {
  $action = $_GET['action'];
}
else {
  error('Calendar action handler', 'No request given' );
}

//what do you want to do today =]
switch($action ) {

  case 'show':
    create_top( $lang['calendar'], 3, 'calendar-show' );
    include(BASE.'calendar/calendar_show.php' );
    create_bottom();
    break;

  case 'date':
    create_top( $lang['calendar'], 3, 'calendar-date', 2 );
    include(BASE.'calendar/calendar_date.php' );
    create_bottom();
    break;

  //error case
  default:
    error('Calendar action handler', 'Invalid request given' );
    break;
}

?>
