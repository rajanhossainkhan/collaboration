<?php
/*
  Author: Rajan Hossain
*/

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//includes
require_once(BASE.'includes/token.php' );

if(isset($_POST['taskid'] ) && safe_integer($_POST['taskid'] ) ) {
  $taskid = $_POST['taskid'];
}
elseif(isset($_GET['taskid'] ) && safe_integer($_GET['taskid'] ) ) {
  $taskid = $_GET['taskid'];
}
else {
  error('Archive submit', 'Not a valid taskid' );
}

//check for valid form token
$token = (isset($_POST['token'])) ? (safe_data($_POST['token'])) : null;

//check if the user has enough rights
if( ! ADMIN ) {
  $q = db_prepare('SELECT COUNT(*) FROM '.PRE.'tasks WHERE id=? AND owner=? LIMIT 1' );
  db_execute($q, array($taskid, UID ) );

  if(db_result($q, 0, 0 ) < 1 ) {
    warning($lang['task_submit'], $lang['not_owner'] );
  }
}

//get projectid
$q = db_prepare('SELECT projectid FROM '.PRE.'tasks WHERE id=? LIMIT 1' );
db_execute($q, array($taskid ) );

if(! ($projectid = db_result($q, 0, 0 ) ) ) {
  error("Archive submit", "Not a valid projectid" );
}

if(isset($_POST['action'] ) ) {
  $action = $_POST['action'];
}
elseif(isset($_GET['action'] ) ) {
  $action = $_GET['action'];
}
else {
  error('Archive submit', 'No valid action set' );
}

switch($action ) {

  case 'submit_archive':

    //do the archiving
    $q = db_prepare('UPDATE '.PRE.'tasks SET archive=1 WHERE projectid=?' );
    db_execute($q, array($projectid ) );

    header('Location: '.BASE_URL.'main.php?x='.X );
    die;
    break;

  case 'submit_restore':

    //do the restore
    $q = db_prepare('UPDATE '.PRE.'tasks SET archive=0 WHERE projectid=?' );
    db_execute($q, array($projectid ) );

    header('Location: '.BASE_URL.'archive.php?x='.X.'&action=list' );
    die;
    break;

    default:
      error('Archive submit', 'Invalid request given');
    break;
}

?>