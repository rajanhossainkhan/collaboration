<?php
/*
  Author : Rajan  Hossain

*/

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//
// Basic query function
//

function icalendar_query() {

  $q =  'SELECT '.PRE.'tasks.id AS taskid,
                '.PRE.'tasks.name AS name,
                '.PRE.'tasks.text AS text,
                '.PRE.'tasks.deadline AS deadline_date,
                ('.PRE.'tasks.deadline+INTERVAL '.db_delim('24 HOUR' ).') AS deadline_date_end,
                ('.PRE.'tasks.created-INTERVAL '.db_delim(TZ.' HOUR' ).') AS created_utc,
                ('.PRE.'tasks.edited-INTERVAL '.db_delim(TZ.' HOUR' ).') AS edited_utc,
                '.PRE.'tasks.status AS status,
                '.PRE.'tasks.priority AS priority,
                '.PRE.'tasks.parent AS parent,
                '.PRE.'tasks.owner AS owner,
                '.PRE.'tasks.usergroupid AS usergroupid,
                '.PRE.'tasks.globalaccess AS globalaccess,
                '.PRE.'tasks.projectid AS projectid,
                '.PRE.'tasks.completed AS completed,
                '.PRE.'tasks.sequence AS sequence,
                '.PRE.'users.id AS userid,
                '.PRE.'users.fullname AS fullname,
                '.PRE.'users.email AS email
                FROM '.PRE.'tasks
                LEFT JOIN '.PRE.'users ON ('.PRE.'users.id='.PRE.'tasks.owner)
                WHERE '.PRE.'tasks.archive=0 ';

  return $q;
}

//
// SQL tail for user access rights
//

function icalendar_usergroup_tail() {

  //set the usergroup permissions on queries (Admin can see all)
  if(ADMIN ) {
    $tail = ' ';
  }
  else {
    $tail = ' AND ('.PRE.'tasks.globalaccess=\'f\' AND '.PRE.'tasks.usergroupid IN (SELECT usergroupid FROM '.PRE.'usergroups_users WHERE userid='.UID.')
              OR '.PRE.'tasks.globalaccess=\'t\'
              OR '.PRE.'tasks.usergroupid=0) ';
  }
  return $tail;
}

?>