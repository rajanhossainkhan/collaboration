<?php
/*
  Author : Rajan  Hossain

*/

//remote login check
if(! isset($local_login) ) {

  //load required files
  require_once('path.php' );
  require_once(BASE.'path_config.php' );
  require_once(BASE_CONFIG.'config.php' );

  include_once(BASE.'includes/common.php');
  include_once(BASE.'database/database.php');
  include_once(BASE.'icalendar/icalendar_login.php' );

  if(! icalendar_login() ) {
    icalendar_error('401', 'Todo login' );
  }
  $remote_login = true;
}

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

include_once(BASE.'icalendar/icalendar_download.php' );
include_once(BASE.'icalendar/icalendar_common.php' );

//set variables
$content  = '';
$icalendar_id = sha1(MANAGER_NAME . BASE_URL );
$dtstamp = gmdate('Ymd\THis\Z');

//main query
$q = db_query(icalendar_query(). icalendar_usergroup_tail() ); 

for($i=0 ; $row = @db_fetch_array($q, $i) ; ++$i ) {

  //add vtodo
  $content .= icalendar_body($row, $row['taskid'] );
}

//no rows ==> return
if(isset($local_login ) && $i == 0 ) {
  header('Location: '.BASE_URL.'main.php?x='.X );
  die;
}

//we have content, send it!

//send headers to browser
icalendar_header('ALL');

//send content
echo $content;

//end of file
icalendar_end();

?>