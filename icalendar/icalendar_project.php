<?php
/*
  Author : Rajan  Hossain
*/

//remote login check
if(! isset($local_login ) ) {

  //load required files
  require_once('path.php' );
  require_once(BASE.'path_config.php' );
  require_once(BASE_CONFIG.'config.php' );

  include_once(BASE.'includes/common.php');
  include_once(BASE.'database/database.php');
  include_once(BASE.'icalendar/icalendar_login.php' );

  if(! icalendar_login() ) {
    icalendar_error('401', 'Todo login' );
  }
}

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

include_once(BASE.'icalendar/icalendar_download.php' );
include_once(BASE.'icalendar/icalendar_common.php' );

//set variables
$content  = '';
$icalendar_id = sha1(MANAGER_NAME . BASE_URL );
$dtstamp = gmdate('Ymd\THis\Z');

if(! @safe_integer($_GET['taskid']) ){
  error('iCalendar show', 'Not a valid value for taskid' );
}
$taskid = $_GET['taskid'];

$q = db_prepare('SELECT COUNT(*) FROM '.PRE.'tasks WHERE id=? AND parent=0' );
db_execute($q, array($taskid ) );

if(db_result($q, 0, 0 ) > 0 ) {
  //project - get all the tasks too...
  $type = 'tasks.projectid=?';
  $id   = 'P';
}
else {
  //task
  $type = 'tasks.id=?';
  $id   = 'T';
}

//main query
$q = db_prepare(icalendar_query().' AND '.PRE.$type. icalendar_usergroup_tail() );
db_execute($q, array($taskid ) );

for($i=0 ; $row = @db_fetch_array($q, $i) ; ++$i ) {

  //add vtodo
  $content .= icalendar_body($row, $row['taskid'] );
}

//no rows ==> return
if(isset($local_login ) && $i == 0 ) {
  header('Location: '.BASE_URL.'tasks.php?x='.X.'&action=show&taskid='.$taskid );
  die;
}

//we have content, send it!

//send headers to browser
icalendar_header($id.$taskid );

//send content...
echo $content;

//end of file
icalendar_end();

?>
