<?php
/*
  Author: Rajan Hossain
  Page: Archive list for deleted project  
*/
require_once('path.php');
require_once(BASE.'includes/security.php' );
include_once(BASE.'includes/screen.php' );

//
// The action handler
//
if(isset($_POST['action'] ) ) {
  $action = $_POST['action'];
}
elseif(isset($_GET['action'] ) ) {
  $action = $_GET['action'];
}
else {
  error('Archive action handler', 'No request given' );
}

//what do you want to archive today =]
switch($action ) {

  //list archived projects
  case 'list':
    create_top($lang['projects'], 0, 'archive-list' );
    include(BASE.'includes/mainmenu.php' );
    if(! GUEST ){
      include(BASE.'tasks/task_menubox.php' );
    }
    include(BASE.'users/user_menubox.php' );
    goto_main();
    include(BASE.'archive/archive_list.php' );
    create_bottom();
    break;

  //printable archive info
  case 'archive_print':
    create_top($lang['projects'], 2 );
    include(BASE.'archive/archive_list.php' );
    create_bottom();
    break;

  //archive project
  case 'submit_archive':
    include(BASE.'archive/archive_submit.php' );
    break;

  //restore archived project
  case 'submit_restore':
    include(BASE.'archive/archive_submit.php' );
    break;

  //Error case
  default:
    error('Archive action handler', 'Invalid request given') ;
    break;
}

?>