<?php
/*
  Author : Rajan  Hossain

*/

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//admins only
if(! ADMIN ){
  error('Unauthorised access', 'This function is for admins only' );
}

//secure values
$content = '';

//add an option to admin files
$content .= "<ul class=\"menu\">\n".
            "<li><a href=\"files.php?x=".X."&amp;action=admin\">".$lang['file_admin']."</a></li>\n".
            "</ul>\n";

//show it
new_box( $lang['files'], $content, 'boxdata-menu', 'head-menu', 'boxstyle-menu' );

?>
