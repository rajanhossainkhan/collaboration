<?php
/*
 * Author: Rajan Hossain
 * Page: Taskgroup management main page
*/

require_once('path.php');
require_once(BASE.'includes/security.php' );
include_once(BASE.'includes/screen.php' );

if(! ADMIN ){
  error('Taskgroup action handler', 'This area for admins only' );
}

//
// The action handler
//
if(isset($_POST['action'] ) ) {
  $action = $_POST['action'];
}
elseif(isset($_GET['action'] ) ) {
  $action = $_GET['action'];
}
else {
 error('Taskgroup action handler', 'No request given');
}

//what do you want to taskgroup today =]
switch($action ) {

  //gives a window and some options to do to the poor 'old taskgroup
  case 'manage':
    create_top($lang['manage_taskgroups'], 0, 'taskgroups-manage' );
    include(BASE.'includes/mainmenu.php' );
    include(BASE.'taskgroups/taskgroup_menubox.php' );
    goto_main();
    include(BASE.'taskgroups/taskgroup_manage.php' );
    create_bottom();
    break;

  //show a taskgroup
  case 'add':
    create_top($lang['add_taskgroup'], 0, 'taskgroups-add', 2 );
    include(BASE.'includes/mainmenu.php' );
    include(BASE.'taskgroups/taskgroup_menubox.php' );
    goto_main();
    include(BASE.'taskgroups/taskgroup_add.php' );
    create_bottom();
    break;

  //show a taskgroup
  case 'edit':
    create_top($lang['edit_taskgroup'], 0, 'taskgroups-edit' );
    include(BASE.'includes/mainmenu.php');
    include(BASE.'taskgroups/taskgroup_menubox.php' );
    goto_main();
    include(BASE.'taskgroups/taskgroup_edit.php' );
    create_bottom();
    break;

  //submit
  case 'submit_edit':
  case 'submit_insert':
  case 'submit_del':
  include(BASE.'taskgroups/taskgroup_submit.php' );
  break;

  //error case
  default:
    error('Taskgroup action handler', 'Invalid request given');
    break;
}

?>