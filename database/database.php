<?php
/*
  Author : Rajan  Hossain

*/

if(! defined('DATABASE_TYPE' ) ) {
  die('Config file not loaded properly for database' );
}

switch(DATABASE_TYPE ) {

  case 'mysql_pdo':
    require(BASE.'database/mysql_pdo.php' );
    break;

  case 'postgresql_pdo':
    require(BASE.'database/postgresql_pdo.php' );
    break;

  default:
    die('No database type specified in configuration file' );
    break;
}

?>
