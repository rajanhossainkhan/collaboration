<?php
/*
  Author: Rajan Hossain
  Page: File management  
*/

require_once('path.php');
require_once(BASE.'includes/security.php' );
include_once(BASE.'includes/screen.php' );

//
// The action handler
//
if(isset($_POST['action'] ) ) {
  $action = $_POST['action'];
}
elseif(isset($_GET['action'] ) ) {
  $action = $_GET['action'];
}
else {
  error('Files action handler', 'No request given' );
}

//what do you want to task today =]
switch($action ) {

  //download a file
  case 'download':
    include(BASE.'files/file_download.php' );
    break;

  //upload a file
  case 'upload':
    create_top($lang['file_choose'], 0, 'file-upload', 1 );
    include(BASE.'includes/mainmenu.php' );
    goto_main();
    include(BASE.'files/file_upload.php' );
    create_bottom();
    break;

  //update a file
  case 'update':
    create_top($lang['file_choose'], 0, 'file-update', 1 );
    include(BASE.'includes/mainmenu.php' );
    goto_main();
    include(BASE.'files/file_update.php' );
    create_bottom();
    break;

  //delete a file
  case 'delete':
    create_top($lang['file_choose'] );
    include(BASE.'includes/mainmenu.php' );
    goto_main();
    include(BASE.'files/file_update.php' );
    create_bottom();
    break;

  //admin files
  case 'admin':
    create_top($lang['file_admin'], 0, 'file-admin' );
    include(BASE.'includes/mainmenu.php' );
    include(BASE.'files/file_menubox.php' );
    goto_main();
    include(BASE.'files/file_admin.php' );
    create_bottom();
    break;

    //search
  case 'search':
    create_top($lang['info'], 0, 'file-search-results' );
    include(BASE.'includes/mainmenu.php');
    //include(BASE.'forum/forum_menubox.php');
    goto_main();
    include(BASE.'files/file_search.php');
    create_bottom();
    break;
    
  //display search box
  case 'search_box':
    create_top($lang['info'], 0, 'file-search', 2 );
    include(BASE.'includes/mainmenu.php');
    //include(BASE.'forum/forum_menubox.php');
    goto_main();
    include(BASE.'files/file_searchbox.php');
    create_bottom();
    break;

  case 'submit_del':
  case 'submit_upload':
  case 'submit_update':
    include(BASE.'files/file_submit.php' );
    break;

  //error case
  default:
    error('File action handler', 'Invalid request given' );
    break;
}

?>