<?php
/*
  Author : Rajan  Hossain
  Manage the task-groups

*/

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//admins only
if( ! ADMIN ){
  error('Unauthorised access', 'This function is for admins only.' );
}

//get the info
$q = db_query('SELECT * FROM '.PRE.'taskgroups ORDER BY name' );

$content =  "<table class=\"celldata\">\n".
            "<tr><th>".$lang['name']."</th><th>".$lang['description']."</th></tr>\n";

//show all taskgroups
for( $i=0 ; $row = @db_fetch_array($q, $i ) ; ++$i ) {
  $content .= "<tr><td colspan=\"3\"><hr /></td></tr>\n".
              "<tr class=\"grouplist\"><td ><b>".$row['name']."</b></td><td><i>".$row['description']."</i></td>".
              "<td><span class=\"textlink\"><a href=\"taskgroups.php?x=".X."&amp;action=edit&amp;taskgroupid=".$row['id']."\">[".$lang['edit']."]</a></span></td></tr>\n";

}

$content .= "</table>\n".
            "<p><span class=\"textlink\">[<a href=\"taskgroups.php?x=".X."&amp;action=add\">".$lang['add']."</a>]</span></p>\n";


//nothing here yet
if($i == 0 ) {
  $content = "<p>".$lang['no_taskgroups']."</p>\n".
             "<span class=\"textlink\"><a href=\"taskgroups.php?x=".X."&amp;action=add\">[".$lang['add']."]</a></span>\n";

  new_box($lang['taskgroup_manage'], $content );
}
else {
  new_box( $lang['manage_taskgroups'], $content, 'boxdata-normal', 'head-normal', 'boxstyle-short' );
}

//admin gets some user notes
include_once(BASE.'lang/lang_long.php' );
$content = $taskgroup_info;
new_box( $lang['info_taskgroup_manage'], $content );

?>
