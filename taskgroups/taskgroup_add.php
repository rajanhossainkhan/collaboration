<?php
/*
 Author : Rajan  Hossain
*/

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//includes
require_once(BASE.'includes/token.php' );

//admins only
if(! ADMIN ){
  error('Unauthorised access', 'This function is for admins only.' );
}

//generate_token
generate_token('taskgroup' );

$content =
      "<form method=\"post\" action=\"taskgroups.php\" onsubmit=\"return fieldCheck('name')\">\n".
      "<fieldset><input type=\"hidden\" name=\"x\" value=\"".X."\" />\n".
      "<input type=\"hidden\" name=\"token\" value=\"".TOKEN."\" />\n".
      "<input type=\"hidden\" name=\"action\" value=\"submit_insert\" />\n".
      "<input type=\"hidden\" id=\"alert_field\" name=\"alert\" value=\"".$lang['missing_field_javascript']."\" /></fieldset>\n".
      "<table class=\"celldata\">\n".
      "<tr><td>".$lang['taskgroup_name']."</td><td><input id=\"name\" type=\"text\" name=\"name\" class=\"size\" />".
      "<script type=\"text/javascript\">document.getElementById('name').focus();</script></td></tr>\n".
      "<tr><td>".$lang['taskgroup_description']."</td><td><input type=\"text\"name=\"description\" class=\"size\" /></td></tr>\n".
      "</table>\n".
      "<p><input type=\"submit\" value=\"".$lang['add_taskgroup']."\" /></p>\n".
      "</form>\n";

new_box( $lang['add_new_taskgroup'], $content );

?>