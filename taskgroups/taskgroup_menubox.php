<?php
/*
Author : Rajan  Hossain

*/

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//only for admins
if(! ADMIN ) {
  return;
}

$content = "<ul class=\"menu\">\n".
           "<li><a href=\"taskgroups.php?x=".X."&amp;action=add\">".$lang['add']."</a></li>\n".
           "<li><a href=\"taskgroups.php?x=".X."&amp;action=manage\">".$lang['manage']."</a></li>\n".
           "</ul>\n";

new_box($lang['taskgroups'], $content, 'boxdata-menu', 'head-menu', 'boxstyle-menu' );

?>
