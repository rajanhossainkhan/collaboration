<?php
/*
 Author : Rajan  Hossain
*/

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

$content = "<ul class=\"menu\">\n";

if(ADMIN ) {
  $content .= "<li><a href=\"usergroups.php?x=".X."&amp;action=add\">".$lang['add']."</a></li>\n";
}

$content .= "<li><a href=\"usergroups.php?x=".X."&amp;action=manage\">".$lang['manage']."</a></li>\n".
            "</ul>\n";

new_box( $lang['usergroups'], $content, 'boxdata-menu', 'head-menu', 'boxstyle-menu' );

?>
