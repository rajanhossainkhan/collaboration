<?php
/*
  Author : Rajan  Hossain
*/

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//secure variables
$content   = '';
$clone     = '';
$archive   = '';
$menu_type = 'project';
$taskid    = -1;

//guests shouldn't get here
if(GUEST ) {
  return;
}

$content .= "<ul class=\"menu\">\n";

//get taskid (if any)
if(isset($_GET['taskid']) && safe_integer($_GET['taskid']) ){

  $taskid = $_GET['taskid'];

  include_once(BASE.'includes/details.php' );

  //don't show options for archived projects
  if($TASKID_ROW['archive'] == 0 ) {

    $menu_type = $TYPE;
    //header
    $content .= "<li><small><b>".$lang['admin'].":</b></small></li>\n";

    //edit rights
    if((ADMIN ) ||
       ($TASKID_ROW['owner'] == UID ) ||
       (($TASKID_ROW['groupaccess'] == "t") && (isset($GID[($TASKID_ROW['usergroupid'])] ) ) ) ) {

      //edit
      $content .= "<li><a href=\"tasks.php?x=".X."&amp;action=edit&amp;taskid=".$taskid."\">".$lang["edit_".$TYPE]."</a></li>\n";

      //archive project
      if((ADMIN ) || ($TASKID_ROW['owner'] == UID ) ) {
        if(($TYPE == 'project' ) && ($TASKID_ROW['archive'] == 0 ) ) {
          $content .= "<li><a href=\"archive.php?x=".X."&amp;action=submit_archive&amp;taskid=".$taskid."\"  onclick=\"return confirm( '".sprintf($lang['javascript_archive_project'], javascript_escape($TASKID_ROW['name'] ) )."')\">".$lang['archive_project']."</a></li>\n";
        }
      }
    }
    //clone
    $content .= "<li><a href=\"tasks.php?x=".X."&amp;action=clone&amp;taskid=".$taskid."\">".$lang["clone_".$TYPE]."</a></li>\n";
    //global header
    $content .= "<li><small><b>".$lang['global'].":</b></small></li>\n";
    //add task
    $content .= "<li><a href=\"tasks.php?x=".X."&amp;action=add&amp;parentid=".$taskid."\">".$lang['add_task']."</a></li>\n";
  }
}
//add project
$content .= "<li><a href=\"tasks.php?x=".X."&amp;action=add\">".$lang['add_project']."</a></li>\n".
            "</ul>\n";

new_box( $lang[$menu_type."_options"], $content, 'boxdata-menu', 'head-menu', 'boxstyle-menu' );

?>
