<?php
/*
  Author : Rajan  Hossain

*/


//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//includes
require_once(BASE.'includes/token.php' );
include_once(BASE.'includes/time.php' );

//guests shouldn't get here
if(GUEST ) {
  warning($lang['access_denied'], $lang['not_owner'] );
}

if(! @safe_integer($_GET['taskid']) ) {
  error('Task clone', 'Taskid not set' );
}

$taskid = $_GET['taskid'];

//generate_token
generate_token('tasks' );

$content = '';

$content .= "<form method=\"post\" action=\"tasks.php\" onsubmit=\"return fieldCheck('name')\">\n".
            "<fieldset><input type=\"hidden\" name=\"x\" value=\"".X."\" />\n ".
            "<input type=\"hidden\" name=\"action\" value=\"submit_clone\" />\n ".
            "<input type=\"hidden\" name=\"taskid\" value=\"".$taskid."\" />\n".
            "<input type=\"hidden\" id=\"token\" name=\"token\" value=\"".TOKEN."\" />\n".
            "<input type=\"hidden\" id=\"alert_field\" name=\"alert\" value=\"".$lang['missing_field_javascript']."\" /></fieldset>\n".
            "<table class=\"celldata\">\n";

$q = db_prepare('SELECT name, parent FROM '.PRE.'tasks WHERE id=?' );
db_execute($q, array($taskid ) );

$row = db_fetch_array($q, 0 );

if($row['parent'] == 0 ){
  $content .= "<tr><td>".$lang['project_cloned']."</td><td><a href=\"tasks.php?x=".X."&amp;action=show&amp;taskid=".$taskid."\">".$row['name']."</a></td></tr>\n".
              "<tr><td>".$lang['project_name'].":</td> <td><input id=\"name\" type=\"text\" name=\"name\" class=\"size\" />".
              "<script type=\"text/javascript\">document.getElementById('name').focus();</script></td></tr>\n".
              "<tr><td>".$lang['deadline'].":</td> <td>".date_select()."</td></tr>\n".
              "</table>\n".
              "<p><input type=\"submit\" value=\"".$lang['add_project']."\" /></p>".
              "</form>\n";

  new_box( $lang['add_project'], $content );

}
else{
  $content .= "<tr><td>".$lang['task_cloned']."</td><td><a href=\"tasks.php?x=".X."&amp;action=show&amp;taskid=".$taskid."\">".$row['name']."</a></td></tr>\n".
              "<tr><td colspan=\"2\"><i>".$lang['note_clone']."</i></td></tr>\n".
              "<tr><td>".$lang['project_name'].":</td> <td><input id=\"name\" type=\"text\" name=\"name\" class=\"size\" /></td> </tr>\n".
              "<tr><td>".$lang['deadline'].":</td> <td>".date_select()."</td></tr>\n".
              "</table>\n".
              "<p><input type=\"submit\" value=\"".$lang['add_project']."\"/></p>".
              "</form>\n";

  new_box( $lang['add_task'], $content );
}

?>