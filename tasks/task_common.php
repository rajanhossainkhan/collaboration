<?php
/*
  Author : Rajan  Hossain
*/

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//
// Gives back the percentage completed of this tasks's children
//
//
function percent_complete($taskid ) {

  $tasks_completed = 0;
  $total_tasks = 0;

  $q = db_prepare('SELECT status FROM '.PRE.'tasks WHERE projectid=? AND parent<>0' );
  db_execute($q, array($taskid ) );

  for($i=0 ; $row = @db_fetch_num($q, $i ) ; ++$i ) { 

    ++$total_tasks;

    if($row[0] == 'done'){
      ++$tasks_completed;
    }
  }

  //project with no tasks is complete
  if($total_tasks == 0 ){
    return 0;
  }

  return round(($tasks_completed / ($total_tasks ) ) * 100 );
}

//
// Show percent
//
function show_percent($percent=0 ) {

  switch($percent ) {
    case '0':
      return "<table class=\"colourbar\"><tr><td style=\"width: 100%\" class=\"redbar\"></td></tr></table>\n";
      break;

    case '100':
      return "<table class=\"colourbar\"><tr><td style=\"width: 100%\" class=\"greenbar\"></td></tr></table>\n";
      break;

    default:
      return "<table class=\"colourbar\"><tr><td style=\"width:".$percent."%\" class=\"greenbar\">".
             "</td><td style=\"width :".(100 - $percent)."%\" class=\"redbar\"></td></tr></table>\n";
      break;
  }
  return;
}

//
// Project Jump function
//
function project_jump($taskid=0) {

  global $lang, $GID;

  $content = '';

  $tail = usergroup_tail();

  //query to get the non-completed projects
  $q = db_query('SELECT id,
                        name,
                        globalaccess,
                        usergroupid,
                        owner
                        FROM '.PRE.'tasks
                        WHERE parent=0
                        AND completed<>100
                        AND archive=0'
                        .$tail.
                        'ORDER BY name' );

  // Prepare the form
  $content .= "<form id=\"ProjectQuickJump\" method=\"get\" action=\"tasks.php\">\n".
              "<fieldset><input type=\"hidden\" name=\"x\" value=\"".X."\" />\n".
              "<input type=\"hidden\" name=\"action\" value=\"show\" /></fieldset>\n".
              "<div><select name=\"taskid\" onchange=\"javascript:this.form.submit()\">\n".
              "<option value=\"-1\">".$lang['quick_jump']."</option>\n";

  // loop through the data
  for( $i=0 ; $row = @db_fetch_array($q, $i ) ; ++$i ){

    $content .= "<option value=\"".$row["id"]."\"";
    if($taskid == $row['id']) {
      $content .= " selected=\"selected\"";
    }
    $content .= ">".$row['name']."</option>\n";
  }

  // wrap up the select and the submit
  $content .= "</select>\n".
              "<a href=\"javascript:void(document.getElementById('ProjectQuickJump').submit())\"><small>".$lang['go']."</small></a></div>\n".
              "</form>\n";

  db_free_result($q );

  //check if there were any results
  if($i == 0 ) {
    $content = '';
  }

  return $content;
}


//
// SQL tail for user access rights
//

function usergroup_tail() {

  //set the usergroup permissions on queries (Admin can see all)
  if(ADMIN ) {
    $tail = ' ';
  }
  else {
    $tail = ' AND ('.PRE.'tasks.globalaccess=\'f\' AND '.PRE.'tasks.usergroupid IN (SELECT usergroupid FROM '.PRE.'usergroups_users WHERE userid='.UID.')
              OR '.PRE.'tasks.globalaccess=\'t\'
              OR '.PRE.'tasks.usergroupid=0) ';
  }
  return $tail;
}

?>